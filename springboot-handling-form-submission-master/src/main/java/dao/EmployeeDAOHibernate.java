package dao;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.google.gson.Gson;
import com.journaldev.hibernate.model.Employee;
import org.hibernate.service.ServiceRegistry;
import org.junit.Test;

public class EmployeeDAOHibernate implements EmployeeDAO{
	private Session currentSession;
	private Transaction currentTransaction;
	private static SessionFactory sessionAnnotationFactory=null;
	
	
	@Override
	public Employee getEmployee(int id) {
		Employee employee=null;
		 try 
	        { 
	            currentSession=openCurrentSessionwithTransaction(); 
	            employee = (Employee) currentSession.get(Employee.class, id); 
         
	        } finally 
	        { 
	        	currentSession.close(); 
	        }  
		
		
		// TODO Auto-generated method stub
		return employee;
	}
	

	@Override
	public List<Employee> getEmployees() {
			List<Employee>employees=null;
			try 
	        { 
	            currentSession=openCurrentSessionwithTransaction(); 
	            employees = currentSession.createQuery("from Employee").list(); 
         
	        } finally 
	        { 
	        	currentSession.close(); 
	        }  
		return employees;
		
	}



	
	@Override
	public int addEmployee(Employee employee) {
		int id=-1;
		
	        try 
	        { 
	        	currentSession=openCurrentSessionwithTransaction(); 
	            id = (int) currentSession.save(employee); 
	            currentSession.getTransaction().commit(); 
	        } catch (HibernateException he) 
	        { 
	             
	            throw he; 
	        } finally 
	        { 
	        	currentSession.close(); 
	        }  

	        return id; 
	}

	@Override
	public int updateEmployee(int id, Employee employee) {
		// TODO Auto-generated method stub
		
		
        try 
        { 
        	currentSession=openCurrentSessionwithTransaction(); 
            currentSession.update(employee); 
            currentSession.getTransaction().commit(); 
        } catch (HibernateException he) 
        { 
             
            throw he; 
        } finally 
        { 
        	currentSession.close(); 
        }  

        return id; 
		
		
		
	}
	
	
	@Override 
	public void deleteEmployee(int id) {
		
		Employee emp=this.getEmployee(id);
		  try 
	        { 
	        	currentSession=openCurrentSessionwithTransaction(); 
	            currentSession.delete(emp); 
	            currentSession.getTransaction().commit(); 
	        } catch (HibernateException he) 
	        { 
	             
	            throw he; 
	        } finally 
	        { 
	        	currentSession.close(); 
	        }  
		
		
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}
	
	public void closeCurrentSession() {
		currentSession.close();
	}
	
	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}
	
	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}


	public static SessionFactory getSessionAnnotationFactory() {
		if(sessionAnnotationFactory == null)
			sessionAnnotationFactory = buildSessionAnnotationFactory();
        return sessionAnnotationFactory;
    }
	
	private static SessionFactory buildSessionAnnotationFactory() {
		
		System.out.println("ENTRO A BUILDSESSIONANNOTATIONFACTORY");
    	try {
            // Create the SessionFactory from hibernate.cfg.xml
        	Configuration configuration = new Configuration();
        	configuration.configure("hibernate-annotation.cfg.xml");
        	System.out.println("Hibernate Annotation Confiernguration loaded");
        	
        	org.hibernate.service.ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        	System.out.println("Hibernate Annotation serviceRegistry created");
        	
        	SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        	
            return sessionFactory;
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
	}

	

	

}
