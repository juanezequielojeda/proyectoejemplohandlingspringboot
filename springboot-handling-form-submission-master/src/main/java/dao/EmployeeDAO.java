package dao;

import java.util.ArrayList;
import java.util.List;

import com.journaldev.hibernate.model.Employee;

public interface EmployeeDAO {
	public Employee getEmployee(int id);

	public int addEmployee(Employee employee);

	public void deleteEmployee(int id);
	public List<Employee> getEmployees();

	public int updateEmployee(int id, Employee employee);
}
