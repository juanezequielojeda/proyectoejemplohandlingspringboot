package com.gf.controller;

import com.gf.entity.Greeting;
import com.journaldev.hibernate.model.Employee;

import dao.EmployeeDAOHibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: GreetingController</p>
 * <p>Description: </p>
 * <p>Company: </p>
 *
 * @author guofu
 * @version 1.0
 * @date 2018-04-10 16:35
 */
@Controller
public class EmployeeController {
	EmployeeDAOHibernate employeeDAOHibernate=new EmployeeDAOHibernate();
	
    @GetMapping("/employee")
    public String greetingFrom(Model model){
        model.addAttribute( "employee" , new Employee() );       
        return "employee";
    }

    @PostMapping("/employee")
    public String greetingSubmit(@ModelAttribute Employee employee){
    	employeeDAOHibernate.addEmployee(employee);
        return "employeeResult";
    }
    
    @RequestMapping("/getEmployee")
    @ResponseBody
    public Employee getEmployee(@RequestParam(value="id",defaultValue="1") int id) {
    	System.out.println("PASO");
    	System.out.println("ID: "+id);
    	return employeeDAOHibernate.getEmployee(id);
    	
    }
    
    
    @RequestMapping("/getEmployees")
    @ResponseBody
    public List<Employee> getEmployees() {
    	System.out.println("PASO");
    	return employeeDAOHibernate.getEmployees();
    	
    	
    }
    @RequestMapping("/deleteEmployee")
    @ResponseBody
    public void deleteEmployee(@RequestParam(value="id",defaultValue="1") int id) {
    	employeeDAOHibernate.deleteEmployee(id);
    	
    }

    @RequestMapping("/updateEmployee")
    @ResponseBody
    public void updateEmployee(@ModelAttribute Employee employee) {
    	System.out.println("PASO");
    	int id=1;
    	Employee emp=new Employee();
    	emp.setId(id);
    	emp.setName("pepito");
    	employeeDAOHibernate.updateEmployee(id,emp);
    	
    }
  
    
    
    

}
