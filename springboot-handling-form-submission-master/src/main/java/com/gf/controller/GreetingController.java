package com.gf.controller;

import com.gf.entity.Greeting;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * <p>Title: GreetingController</p>
 * <p>Description: </p>
 * <p>Company: </p>
 *
 * @author guofu
 * @version 1.0
 * @date 2018-04-10 16:35
 */
@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greetingFrom(Model model){
        model.addAttribute( "greeting" , new Greeting() );       
        return "greeting";
    }

    @PostMapping("/greeting")
    public String greetingSubmit(@ModelAttribute Greeting greeting){
        return "result";
    }
    
    
    
    @GetMapping("/pruebag")
    public String pruebaFrom(Model model){
        model.addAttribute( "greeting" , new Greeting() );       
        return "pruebaGreating";
    }
    
    @PostMapping("/pruebag")
    public String greetingSubmit2(@ModelAttribute Greeting greeting){
        return "pruebaResult";
    }

}
