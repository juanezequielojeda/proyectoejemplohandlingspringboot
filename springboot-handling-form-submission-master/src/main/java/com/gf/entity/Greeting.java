package com.gf.entity;

import java.io.Serializable;

/**
 * <p>Title: Greeting</p>
 * <p>Description: </p>
 * <p>Company: </p>
 *
 * @author guofu
 * @version 1.0
 * @date 2018-04-10 16:33
 */
public class Greeting implements Serializable{

    private static final long serialVersionUID = -5723523941411138694L;

    private long id;
    private String content;
    private String name;

    public long getId() {
        return id;
    }
    
    public String getName() {
    	return this.name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    public void setName(String name) {
    	this.name=name;
    }

}
